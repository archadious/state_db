
#
# File: states.py
# Auth: Archadious
#


# Modules
import pickle
from state import State


# Wrapper around dictionary of states
class StateVector :

  def __init__( self ) :
    self.vector = {}

  def load( self ) :
    print( "DB: StateVector.load" )
    self.vector = pickle.load( open( "state_db.p", "rb" ) )

  def save( self ) :
    print( "DB: StateVector.save" )
    pickle.dump( self.vector, open( "state_db.p", "wb" ) )

  def add( self ) :
    print( "DB: StateVector.add" )
    name = input( "Enter name of state: " ).strip()
    year = int( input( "Enter year state entered union: " ) )
    self.vector[ name ] = State( name, year )

  def list( self ) :
    print( "DB: StateVector.list" )
    if len( self.vector ) > 0 :
      for state in self.vector :
        print( str( self.vector[ state ] ) )
    else :
      print( "No states in database" )
