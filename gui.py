#!/usr/bin/env python3
# Time-stamp: <2022-07-21 10:44:00 daniel>
#
# File: gui.py
# Auth: Archadious
#
# Display all states in a tkinter window
#


# Modules
import tkinter as tk
import pickle


# Custom label
class State_Label :

  # label class constructor
  def __init__( self ) :
    self.label_text = tk.StringVar()  # tk variable to hold the text of this label
    self.label = tk.Label(
      textvariable = self.label_text,
      font = ( "Arial", 32 ),
      width = 50,
      height = 15
      ) # end tk.Label
    self.pack()

  def pack( self ) :
    self.label.pack()

  def set( self, state ) :
    self.label_text.set( state )


# Custom button
class State_Button :

  # button class constructor
  def __init__( self, parent, callback ) :
    self.button = tk.Button(
      parent,
      text = 'Next State',
      command = callback,
      font = ( "Arial", 24 ),
      bg = "red",
      fg = 'blue',
      width = 12,
      height = 3
      )  # end tk.Button
    self.button.pack( pady = 15 )  # add additional space around the button


# Main class of script
class App :

  # application constructor
  def __init__( self ) :
    self.window = tk.Tk()
    self.window.title( "State Database" )
    self.state_vec = pickle.load( open( 'state_db.p', 'rb' ) )
    self.state_names = list( self.state_vec.keys() )
    self.index = 0
    self.label = State_Label()
    self.button = State_Button( self.window, self.next_state )

  # display the next state
  def next_state( self ) :
    if self.index >= len( self.state_names ) :
      self.index = 0
    name = self.state_names[ self.index ]
    self.label.set( self.state_vec[ name ] )
    self.index = self.index + 1

  # run the script
  def main( self ) :
    self.next_state()
    self.window.mainloop()


# Entry into script
if __name__ == "__main__" :
  App().main()
