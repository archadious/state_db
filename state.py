
#
# File: state.py
# Auth: Archadious
#

# Wrapper around a state
class State :

  def __init__( self, name = None, year = 0 ) :
    self.name = name
    self.year = year

  def __str__( self ) :
    return f"{self.name} joined the Union in the year {self.year}"
