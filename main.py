#!/usr/bin/env python
# Time-stamp: <2022-07-21 09:20:18 daniel>
#
# File: main.py
# Auth: Archadious
#

# modules
from application import Application

# Entry into script
if __name__ == "__main__" :
  Application().main()
