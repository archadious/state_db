
#
# File: application.py
# Auth: Archadious
#

# Modules
from state_vector import StateVector


# Wrapper around main object of this script
class Application :

  # Constructor
  def __init__( self ) :
    print( 'DB: Application.__init__' )
    self.vector = StateVector()  # create a states object
    self.command_vector = {
      'load': self.vector.load,
      'save': self.vector.save,
      'add': self.vector.add,
      'list': self.vector.list
    }  # end command_vector

  # read and execute user commands
  def run( self ) :
    print( 'DB: Application.commands' )
    while True :
      cmd = input( "CMD> " ).strip().lower()
      print( f"DB: cmd is '{cmd}'" )
      if cmd == 'q' :
        break
      if cmd in self.command_vector :
        self.command_vector[ cmd ]()
      else :
        print( f"'{cmd}' is not known" )

  # Entry into application
  def main( self ) :
    print( 'DB: Application.main' )
    self.run()
